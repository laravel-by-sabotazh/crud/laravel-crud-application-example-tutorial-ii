<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return to_route('products.index');
});

Route::resource('products', ProductController::class);
